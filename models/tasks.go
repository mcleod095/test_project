package models

import (
	"github.com/prometheus/client_golang/prometheus"
	"time"
)

var (
	db_requests = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "db_requests_total",
		Help: "Number of db requests",
	},
		[]string{"method"})
	db_errors = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "db_errors_total",
		Help: "Number of db errors",
	},
		[]string{"method"})

	db_requests_histogram = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "db_request_duration_seconds",
		Help: "Time taken to db request",
	}, []string{"method"})
)

func init() {
	prometheus.Register(db_requests_histogram)
	prometheus.Register(db_requests)
	prometheus.Register(db_errors)
}

type Task struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type TaskCollection struct {
	Tasks []Task `json:"items"`
}

func (db *DB) GetTasks() (TaskCollection, error) {
	const query = `SELECT * FROM tasks`
	label := prometheus.Labels{"method": "GetTasks"}
	start := time.Now()

	rows, err := db.DB.Query(query)

	if err != nil {
		db_errors.With(label).Inc()
		return TaskCollection{}, err
	}
	defer rows.Close()

	result := TaskCollection{}
	for rows.Next() {
		task := Task{}
		err := rows.Scan(&task.ID, &task.Name)
		if err != nil {
			db_errors.With(label).Inc()
			return TaskCollection{}, err
		}
		result.Tasks = append(result.Tasks, task)
	}
	duration := time.Since(start)
	db_requests.With(label).Inc()
	db_requests_histogram.With(label).Observe(duration.Seconds())
	return result, nil
}

func (db *DB) PutTask(id int64, name string) (int64, error) {
	const query = `UPDATE tasks SET name=? WHERE id=?`
	label := prometheus.Labels{"method": "PutTask"}
	start := time.Now()

	stmt, err := db.DB.Prepare(query)
	if err != nil {
		db_errors.With(label).Inc()
		return 0, err
	}
	defer stmt.Close()

	result, err := stmt.Exec(name, id)
	if err != nil {
		db_errors.With(label).Inc()
		return 0, err
	}
	duration := time.Since(start)
	db_requests.With(label).Inc()
	db_requests_histogram.With(label).Observe(duration.Seconds())
	return result.RowsAffected()
}

func (db *DB) PostTask(name string) (int64, error) {
	const query = `INSERT INTO tasks(name) VALUES(?)`
	label := prometheus.Labels{"method": "PostTask"}
	start := time.Now()

	stmt, err := db.DB.Prepare(query)
	if err != nil {
		db_errors.With(label).Inc()
		return 0, err
	}
	defer stmt.Close()

	result, err := stmt.Exec(name)
	if err != nil {
		db_errors.With(label).Inc()
		return 0, err
	}
	duration := time.Since(start)
	db_requests.With(label).Inc()
	db_requests_histogram.With(label).Observe(duration.Seconds())
	return result.LastInsertId()
}

func (db *DB) DeleteTask(id int64) (int64, error) {
	const query = `DELETE FROM tasks WHERE id = ?`
	label := prometheus.Labels{"method": "DeleteTask"}
	start := time.Now()

	stmt, err := db.DB.Prepare(query)
	if err != nil {
		db_errors.With(label).Inc()
		return 0, err
	}
	defer stmt.Close()

	result, err := stmt.Exec(id)
	if err != nil {
		db_errors.With(label).Inc()
		return 0, err
	}
	duration := time.Since(start)
	db_requests.With(label).Inc()
	db_requests_histogram.With(label).Observe(duration.Seconds())
	return result.RowsAffected()
}
