package models

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

type Datastore interface {
	GetTasks() (TaskCollection, error)
	PutTask(int64, string) (int64, error)
	DeleteTask(int64) (int64, error)
	PostTask(string) (int64, error)
}

type DB struct {
	*sql.DB
}

func NewModel(dsn string) (*DB, error) {
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}

	return &DB{db}, nil
}

func CreateTasksTable(db *sql.DB) error {
	const query = `CREATE TABLE IF NOT EXISTS tasks(
		id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
		name VARCHAR(255) NOT NULL UNIQUE
	);
	`
	stmt, err := db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec()
	if err != nil {
		return err
	}
	return nil
}

func (db *DB) InitDB() error {
	if err := CreateTasksTable(db.DB); err != nil {
		return err
	}
	return nil
}
