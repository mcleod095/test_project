package main

import (
	mw "OPR-examples/middleware"
	"OPR-examples/models"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
	"net/http"
	"strconv"
)

type Env struct {
	db models.Datastore
}

func main() {
	//	db, err := models.NewModel("taskuser:taskpass@tcp(mysql:3306)/taskstest?charset=utf8")
	db, err := models.NewModel("app:hYkhqRUzNXtPVQfZWivd7JAa@tcp(mysql-app:3306)/app?charset=utf8")
	if err != nil {
		panic(err)
	}

	if err := db.InitDB(); err != nil {
		panic(err)
	}
	env := &Env{db}

	e := echo.New()
	e.Use(mw.Scrap())
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/metrics", echo.WrapHandler(promhttp.Handler()))

	e.GET("/tasks", env.GetTasks())
	e.POST("tasks", env.PostTask())
	e.PUT("/tasks/:id", env.PutTask())
	e.DELETE("/tasks/:id", env.DeleteTask())

	e.Logger.Fatal(e.Start(":8000"))
}

func (e *Env) GetTasks() echo.HandlerFunc {
	return func(c echo.Context) error {
		tasks, err := e.db.GetTasks()
		if err != nil {
			//c.Error(err)
			return err
		}
		return c.JSON(http.StatusOK, tasks)
	}
}

func (e *Env) PutTask() echo.HandlerFunc {
	return func(c echo.Context) error {
		var task models.Task
		if err := c.Bind(&task); err != nil {
			log.Println(err)
			return err
		}

		id, err := strconv.ParseInt(c.Param("id"), 10, 64)
		if err != nil {
			log.Println(err)
			return err
		}
		id, err = e.db.PutTask(id, task.Name)
		if err != nil {
			log.Println(err)
			return err
		}
		return c.JSON(http.StatusOK, map[string]interface{}{"updated": id})
	}
}

func (e *Env) PostTask() echo.HandlerFunc {
	return func(c echo.Context) error {
		var task models.Task
		if err := c.Bind(&task); err != nil {
			log.Println(err)
			return err
		}
		id, err := e.db.PostTask(task.Name)
		if err != nil {
			log.Println(err)
			return err
		}
		return c.JSON(http.StatusOK, map[string]interface{}{"createdid": id})
	}
}

func (e *Env) DeleteTask() echo.HandlerFunc {
	return func(c echo.Context) error {
		id, err := strconv.ParseInt(c.Param("id"), 10, 64)
		if err != nil {
			log.Println(err)
			return err
		}

		count, err := e.db.DeleteTask(id)
		if err != nil {
			log.Println(err)
			return err
		}
		return c.JSON(http.StatusOK, map[string]interface{}{"deleted": count})
	}
}
