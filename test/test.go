package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"github.com/satori/go.uuid"
)

type Task struct {
	Name string
	Id   int64
}

func CreateTask(wg *sync.WaitGroup, r <-chan string, w chan<- Task) {
	defer wg.Done()
	hc := http.Client{}
	for {
		u, ok := <-r
		if !ok {
			close(w)
			return
		}
		body := "{\"name\":\"" + u + "\"}"
		req, err := http.NewRequest("POST", url+"/tasks", strings.NewReader(body))
		if err != nil {
			return
		}
		req.Header.Set("Content-type", "application/json")
		resp, err := hc.Do(req)
		if err != nil {
			fmt.Println("ERROR", u, err)
			continue
		}
		if resp.StatusCode == http.StatusOK {
			data, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				fmt.Println(err)
				continue
			}
			var r map[string]int64
			err = json.Unmarshal(data, &r)
			if err != nil {
				fmt.Println(err)
				continue
			}
			w <- Task{Name: u, Id: r["createdid"]}
		}
		resp.Body.Close()
	}

}

func EditTask(wg *sync.WaitGroup, r <-chan Task, w chan<- Task) {
	defer wg.Done()
	hc := http.Client{}
	for {
		m, ok := <-r
		if !ok {
			close(w)
			return
		}
		body := "{\"name\":\"rename_" + m.Name + "\"}"
		req, err := http.NewRequest("PUT", url+"/tasks/"+strconv.FormatInt(m.Id, 10), strings.NewReader(body))
		if err != nil {
			fmt.Println(err)
			continue
		}
		req.Header.Set("Content-type", "application/json")
		resp, err := hc.Do(req)
		if err != nil {
			fmt.Println(err)
			continue
		}
		if resp.StatusCode == http.StatusOK {
			data, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				fmt.Println(err)
				continue
			}
			var r map[string]int64
			err = json.Unmarshal(data, &r)
			if err != nil {
				fmt.Println(err)
				continue
			}
			m.Name = "rename_" + m.Name
			w <- m
		}
		resp.Body.Close()
	}
}

func DeleteTask(wg *sync.WaitGroup, r <-chan Task) {
	defer wg.Done()
	hc := http.Client{}
	for {
		m, ok := <-r
		if !ok {
			return
		}
		req, err := http.NewRequest("DELETE", url+"/tasks/"+strconv.FormatInt(m.Id, 10), nil)
		if err != nil {
			fmt.Println(err)
			continue
		}
		resp, err := hc.Do(req)
		if err != nil {
			fmt.Println(err)
			continue
		}
		if resp.StatusCode == http.StatusOK {
			fmt.Println("Delete OK")
		}
		resp.Body.Close()
	}
}

var (
	//url = "http://172.21.14.5:8000"
	//url = "http://127.0.0.1:8000"
	url = "http://prometheus01.tema:8000"
)

func main() {
	var wg sync.WaitGroup
	create := make(chan string)
	check := make(chan Task)
	deletech := make(chan Task)
	for i := 0; i < 10; i++ {
		wg.Add(3)
		go CreateTask(&wg, create, check)
		go EditTask(&wg, check, deletech)
		go DeleteTask(&wg, deletech)
	}

	for i := 0; i < 1000; i++ {
		u := uuid.NewV4()
		create <- u.String()
	}
	close(create)
	log.Println("Waiting")
	wg.Wait()
}
