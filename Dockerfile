FROM golang:latest

ENV APP_PATH /app
ENV SRC_PATH /src/OPR-examples
ENV GOPATH $GOPATH:/

RUN mkdir -p $APP_PATH
RUN mkdir -p $SRC_PATH

COPY . $SRC_PATH

WORKDIR $SRC_PATH

RUN go get -u github.com/golang/dep/cmd/dep
RUN pwd; ls -la
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o $APP_PATH/app

ENTRYPOINT $APP_PATH/app
