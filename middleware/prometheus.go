package middleware

import (
	"github.com/labstack/echo"
	"github.com/prometheus/client_golang/prometheus"
	"strconv"
	"time"
	"strings"
)

var (
	http_requests_histogram = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "http_request_duration_seconds",
		Help: "Time taken to htpp request",
	}, []string{"method", "code", "host", "uri"})

	http_requests = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "Number of http requests",
	},
		[]string{"method", "code", "host", "uri"})
)

func init() {
	prometheus.Register(http_requests_histogram)
	prometheus.Register(http_requests)
}

func Scrap() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {
			req := c.Request()
			res := c.Response()
			start := time.Now()
			if err = next(c); err != nil {
				c.Error(err)
			}
			duration := time.Since(start)
			uri := req.RequestURI
			if strings.LastIndex(req.RequestURI, "/") != 0 {
				uri = req.RequestURI[0:strings.LastIndex(req.RequestURI, "/")]
			}
			responseStatus := strconv.Itoa(res.Status)
			labels := prometheus.Labels{"uri": uri, "host": req.Host, "method": req.Method, "code": responseStatus}
			http_requests_histogram.With(labels).Observe(duration.Seconds())
			http_requests.With(labels).Inc()
			return
		}
	}
}
